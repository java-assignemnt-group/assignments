//***************************************************************************************************** 
// 
//  File:           Assignments
//   
//  Student:        Cyrus Bajracharya
// 
//  Assignment:     Assignment  1
// 
//  Course Name:    Java I  
// 
//  Course Number:  CodeForce360 
// 
//  Due:            
//
//  Other files required:  
//   1. CustomerDBMS.java
//   2. CustomerApplication.java
//   3. Validator.java
//   
//***************************************************************************************************** 

package assignment1;
import java.util.LinkedList;
import java.sql.ResultSet;

//*****************************************************************************************************

public class Assignment1 
{
    public static void main(String[] args) 
    {        
        //1. First repeating element in the array
        int[] arr1 = {6, 10, 5, 4, 9, 120, 4, 6, 10};        
        //RepeatingElements.firstRepeat(arr1, arr1.length);
        
        //2. Frequency of elements in a Given String
        String freq = "Iseedeadpeople";           
        //ElementFrequency.getFreq(freq);
        
        //3. Implement Depth First Search and Breadth First Search Algorithm
        
        //4. Write a java program to implement Merge Sort, Insertion Sort, Heap Sort
        int[] arr2 = {6, 10, 5, 4, 9, 12, 1, 3, 2}; 
        //SortAlgorithm.InsertionSort(arr2, arr2.length);
        //SortAlgorithm.MergeSort(arr2, arr2.length);
        //SortAlgorithm.HeapSort(arr2, arr2.length);
        
        //5. Write a Java program for Binary Search and Linear Search
        int[] arr3 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        //SearchAlgorithm.BinarySearch(arr3, arr3.length, 5);
        //SearchAlgorithm.LinearSearch(arr3, arr3.length, 5);
        
        //6. Write a Java Program for finding the maximum sum and target sum of a sub-array in a given set of integers
        int[] arr4  = {4, -3, 1, -5, 10, 30, -2, 6, 7, -9, 8};
        // SumOfSubArray.MaxSelectedSum(arr4, 4, 0);
        // SumOfSubArray.SelectedSum(arr4, 4, 0);
        
        //7. Implement a Linked List and a Double Linked List. 
        LinkedList<String> l1 = new LinkedList<String>();
        //l1.add("hi"); l1.add("my"); l1.add("name"); l1.add("is"); l1.add("cyrus");
        //System.out.println(l1);
     
        
        //8. Write a Java Program to implement Binary Search Tree
        //9. Build a simple REST API that takes your name and DOB returns a response with your Age and Name.
        //10. Write JUnit test cases and Mockito cases for corresponding methods in the previous problems
        
        //11. Create two tables One Employee and One Department 
        Database db1 = new Database("jdbc:derby://localhost:1527/db1");
        //db1.create();
        //db1.populateDefault();
        
        //12. Getting data from an Employee Table with highest salary
        //db1.MaxSalary();
        
        //13. Getting Data from an Employee Table with Highest salary in the Respective Department
        //db1.MaxSalaryPerDepartment();
        
        //14. SQL Query for nth highest Salary
        //db1.nthSalary(1);
        
        //15. SQL Query for joining two tables Employee and Department
        //db1.join();
        
        //16. SQL Query for getting the maximum number of employees are present in which Department
        //db1.EmpPerDepartment();
        
        //17. Implement all the Joins on the two tables.
        //db1.joinAll();
    }
    
    //*****************************************************************************************************
}

//*****************************************************************************************************