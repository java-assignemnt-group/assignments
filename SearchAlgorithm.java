
package assignment1;

public class SearchAlgorithm 
{
    public static int binary(int[] arr, int l, int r, int x)
    {
        if (r >= l) 
        {
            int mid = l + (r - l) / 2;
            if (arr[mid] == x)
                return mid;
            if (arr[mid] > x)
                return binary(arr, l, mid - 1, x);
            return binary(arr, mid + 1, r, x);
        }
        return -1;
    }
    public static void BinarySearch(int[] arr, int size, int target)
    {
        int i = binary(arr, 0, size-1, target);
        if (i != -1)
            System.out.printf("Value: %d found in position: %d.\n", target, i);
        else
            System.out.println("Value not in list.");
    }
    
    public static void LinearSearch(int[] arr, int size, int target)
    {
        boolean found = false;
        int i=0;
        while (i< size && !found)
        {
            if (arr[i] == target)
            {
                System.out.printf("Value: %d found in position: %d.\n", target, i);
                found = true;
            }
            else
                i++;
        }
        if (!found)
            System.out.println("Value not in list.");
    }
}
