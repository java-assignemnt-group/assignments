
package assignment1;

public class SortAlgorithm 
{
    public static void print(String str, int[] arr, int size)
    {
        System.out.print(str +"| ");
        for (int i=0; i<size; i++)
        {
            System.out.print(arr[i] +" | ");
        }
        System.out.println();
        
    }
    
    public static void InsertionSort(int[] arr, int size)
    {
        print("Before Insertion Sort: ", arr, size);
        int hold, j;
        for (int i = 1; i < size; ++i) 
        {
            hold = arr[i];
            j = i - 1;
            while (j >= 0 && arr[j] > hold) 
            {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
             arr[j + 1] = hold;
        }        
        print("After Insertion Sort: ", arr, size);
    }
    
    public static void merge(int arr[], int l, int m, int r)
    {
        int n1 = m - l + 1;
        int n2 = r - m;
  
        int L[] = new int[n1];
        int R[] = new int[n2];
  
        for (int i = 0; i < n1; ++i)
            L[i] = arr[l + i];
        for (int j = 0; j < n2; ++j)
            R[j] = arr[m + 1 + j];
  
        
        int i = 0, j = 0, k = l;
        while (i < n1 && j < n2) 
        {
            if (L[i] <= R[j]) 
            {
                arr[k] = L[i];
                i++;
            }
            else 
            {
                arr[k] = R[j];
                j++;
            }
            k++;
        }  
        
        while (i < n1) 
        {
            arr[k] = L[i];
            i++;
            k++;
        }  
        
        while (j < n2) 
        {
            arr[k] = R[j];
            j++;
            k++;
        }
    }
    public static void sort(int arr[], int l, int r)
    {
        if (l < r) 
        {
            int m =l+ (r-l)/2;
            sort(arr, l, m);
            sort(arr, m + 1, r);
            merge(arr, l, m, r);
        }
    }
    public static void MergeSort(int[] arr, int size)
    {
        print("Before Merge Sort: ", arr, size);     
        sort(arr, 0, arr.length-1);
        print("After Merge Sort: ", arr, size);
    
    }
    public static void heapify(int arr[], int n, int i)
    {
        int largest = i; 
        int l = 2 * i + 1;
        int r = 2 * i + 2; 
 
        if (l < n && arr[l] > arr[largest])
            largest = l;
        if (r < n && arr[r] > arr[largest])
            largest = r;
        if (largest != i) 
        {
            int swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;
            
            heapify(arr, n, largest);
        }
    }
    public static void HeapSort(int[] arr, int size)
    {
        print("Before Merge Sort: ", arr, size);
        int n = arr.length;
        
        for (int i = n / 2 - 1; i >= 0; i--)
            heapify(arr, n, i);
        for (int i = n - 1; i > 0; i--) 
        {            
            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;
            
            heapify(arr, i, 0);
        }        
        print("After Merge Sort: ", arr, size);
    }
}
