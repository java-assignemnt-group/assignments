
package assignment1;

public class SumOfSubArray 
{
    public static boolean CheckSize(int[] arr, int max, int min)
    {
        if (max > arr.length - 1 || min >= max || min < 0)
            return false;
        else 
            return true;
    }
    
    public static void MaxSelectedSum(int[] arr, int max, int min)
    {
        if(!CheckSize(arr, max, min))
            System.out.println("Invalid indexes are provided.");
        else
        {
            int sum = 0;
            for (int i=min; i<=max; i++)
            {
                if(arr[i] > 0)
                    sum += arr[i];
            }
            System.out.println("The maximum sum from "+min+" to "+max+" is "+ sum);
        }
    }
    
    public static void SelectedSum(int[] arr, int max, int min)
    {
        if(!CheckSize(arr, max, min))
            System.out.println("Invalid indexes are provided.");
        else
        {
            int sum = 0;
            for (int i=min; i<=max; i++)
                    sum += arr[i];
            System.out.println("The selected sum from "+min+" to "+max+" is "+ sum);
        }
    }
}
