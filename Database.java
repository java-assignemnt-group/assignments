package assignment1;

import java.sql.*;

public class Database {

    private String DB_URL;
    private Connection conn;
    private Statement stnt;

    Database(String url) {
        DB_URL = url;
    }

    public ResultSet execute(String sql, int option) {
        ResultSet result = null;
        try {
            conn = DriverManager.getConnection(DB_URL);
            stnt = conn.createStatement();
            if (option == 1) {
                stnt.executeUpdate(sql);
            } else if (option == 2) {
                result = stnt.executeQuery(sql);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return result;
    }

    public void close() {
        if (stnt != null) {
            try {
                stnt.close();
                conn.close();
            } catch (SQLException ex) {
                System.out.println("Could not close query.");
            }
        }
    }

    public void create() {
        ResultSet result;
        String sql1 = "CREATE TABLE Employee("
                + "EmployeeName VARCHAR(25) NOT NULL, "
                + "EmployeeID VARCHAR(125) NOT NULL PRIMARY KEY, "
                + "DepartmentID VARCHAR(125) NOT NULL, "
                + "Salary DOUBLE)";
        String sql2 = "CREATE TABLE Department ("
                + "DepartmentID VARCHAR(125) NOT NULL PRIMARY KEY, "
                + "DepartmentName VARCHAR(125) NOT NULL)";
        result = execute(sql1, 1);
        result = execute(sql2, 1);
        close();
    }

    public void populateDefault() {
        ResultSet result;
        result = execute("insert into Employee (EmployeeName, EmployeeID, DepartmentID, Salary) values ('Joleen', '1', '1', 935)", 1);
        result = execute("insert into Employee (EmployeeName, EmployeeID, DepartmentID, Salary) values ('Blondy', '2', '2', 902)", 1);
        result = execute("insert into Employee (EmployeeName, EmployeeID, DepartmentID, Salary) values ('Florida', '3', '3', 960)", 1);
        result = execute("insert into Employee (EmployeeName, EmployeeID, DepartmentID, Salary) values ('Cash', '4', '4', 938)", 1);
        result = execute("insert into Employee (EmployeeName, EmployeeID, DepartmentID, Salary) values ('Margaux', '5', '5', 911)", 1);
        result = execute("insert into Department (DepartmentName, DepartmentID) values ('Human Resources', '1')", 1);
        result = execute("insert into Department (DepartmentName, DepartmentID) values ('Legal', '2')", 1);
        result = execute("insert into Department (DepartmentName, DepartmentID) values ('Accounting', '3')", 1);
        result = execute("insert into Department (DepartmentName, DepartmentID) values ('Research and Development', '4')", 1);
        result = execute("insert into Department (DepartmentName, DepartmentID) values ('Sales', '5')", 1);
        close();
    }

    public void MaxSalary() {
        try {
            ResultSet result = execute("Select * from Employee where salary = (Select MAX(Salary) from Employee)", 2);
            result.next();
            System.out.printf("Person with The highest salary: \nName: %s, ID: %s, DepID: %s, Salary: %.2f\n",
                    result.getString("EmployeeName"), result.getString("EmployeeID"),
                    result.getString("DepartmentID"), result.getDouble("Salary"));
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            close();
        }
    }

    public void MaxSalaryPerDepartment() {
        try {
            ResultSet result1, result2;
            result1 = execute("Select DepartmentID from Department", 2);
            while (result1.next()) {
                String sql = "SELECT * FROM Employee WHERE SALARY IN (SELECT * FROM Employee WHERE DepartmentID = "
                        + result1.getString("DepartmentID") + ")";
                /*String sql = "SELECT DepartmentID, EmployeeName, EmployeeID, Salary FROM Employee "
                        + "WHERE (DepartmentID, Salary) IN (SELECT DepartmentID, MAX(Salary) FROM Employee GROUP BY DepartmentID))";*/
                System.out.println(sql);
                //result2 = execute(sql, 2);
                //result2.next();
                //System.out.printf("Person with The highest salary: \nName: %s, ID: %s, DepID: %s, Salary: %.2f\n", 
                //    result2.getString("EmployeeName"), result2.getString("EmployeeID"), 
                //    result2.getString("DepartmentID"), result2.getDouble("Salary"));                
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            close();
        }
    }

    public void nthSalary(int sal) {

        try {
            int n = sal;
            ResultSet result = execute("Select * from Employee ORDER BY Salary DESC", 2);
            while (result.next()) {
                sal -= 1;
                if (sal <= 0) {
                    System.out.printf("Person with The %dth highest salary: \nName: %s, ID: %s, DepID: %s, Salary: %.2f\n",
                            n, result.getString("EmployeeName"), result.getString("EmployeeID"),
                            result.getString("DepartmentID"), result.getDouble("Salary"));
                    break;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            close();
        }
    }

    public void join() {
        try {
            ResultSet result = execute("Select * from Employee Inner join Department "
                    + "on Employee.DepartmentID = Department.DepartmentID", 2);
            while (result.next()) {
                System.out.printf("\nName: %s, ID: %s, DepartmentName: %s, DepartmentID: %s, Salary: %.2f\n",
                        result.getString("EmployeeName"), result.getString("EmployeeID"),
                        result.getString("DepartmentName"),
                        result.getString("DepartmentID"), result.getDouble("Salary"));
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            close();
        }
    }

    public void joinAll() {
        try {
            ResultSet result = execute("Select * from Employee Inner join Department "
                    + "on Employee.DepartmentID = Department.DepartmentID", 2);
            while (result.next()) {
                System.out.printf("\nName: %s, ID: %s, DepartmentName: %s, DepartmentID: %s, Salary: %.2f\n",
                        result.getString("EmployeeName"), result.getString("EmployeeID"),
                        result.getString("DepartmentName"),
                        result.getString("DepartmentID"), result.getDouble("Salary"));
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            close();
        }
    }

    public void EmpPerDepartment() {
        try {
            ResultSet result1, result2;
            result1 = execute("Select DepartmentID from Department", 2);
            while (result1.next()) {
                String sql = "Select COUNT(EmployeeID) FROM Employee WHERE DepartmentID = '"
                        + result1.getString("DepartmentID") + "'";
                result2 = execute(sql, 2);
                result2.next();
                System.out.printf("Department: %s, Employees in Department: %d\n",
                        result1.getString("DepartmentID"), result2.getInt(1));
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            close();
        }
    }
}
