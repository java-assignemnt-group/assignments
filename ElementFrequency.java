
package assignment1;


public class ElementFrequency 
{
    public static void getFreq(String input)
    {
        int[] count = new int[26];
        input = input.toLowerCase();
        for (int i=0; i<input.length();i++)
        {
            if (Character.isAlphabetic(input.charAt(i)))
            count[input.charAt(i)-96] += 1;
        }
        System.out.print("Words used: ");
        for (int i=0; i<26; i++)
        {
            if (count[i] > 0)
            {
                char c = (char)(i+96);
                System.out.printf(" %d%c", count[i], c);
            }            
        }
        System.out.println();
    }
}
